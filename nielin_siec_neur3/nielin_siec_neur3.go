package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func normalize(vector []float64 ) []float64 {
	length := 0.0
	normalized := vector
	for _, v := range vector {
		length += v*v
	}
	length = math.Sqrt(length)
	for i, v := range normalized {
		normalized[i] = v / length
	}
	return normalized
}

func calcOut(weights, pattern []float64) float64 {
	ret := 0.0
	for i, v := range pattern {
		ret += v * weights[i]
	}
	ret += weights[len(weights)-1]

	if ret > 0 {
		return 1
	} else {
		return -1
	}
}

func printWeights(weights []float64) {
	fmt.Print("Wagi ")
	for i := range weights {
		fmt.Printf("%.3f ", weights[i])
	}
	fmt.Printf("\n")
}

func learn(wRange, learningRate float64) [][]float64 {

	patterns := [][]float64{{-1,1}, {5,-5}, {7,-7}}
	targets := [][]float64{{1,-1,-1}, {-1,1,-1}, {-1,-1,1}}
	weights := [][]float64{{1,1,1}, {1,1,1}, {1,1,1}}
	/*for i := range targets {
		for j := range targets[i] {
			weights[i][j] = (rand.Float64() * 2 * wRange) - wRange
		}
	}*/

	fmt.Println(patterns)
	fmt.Println(targets)
	for i := range weights {
		printWeights(weights[i])
	}

	var c uint8
	fmt.Scanf("%c", &c)
	epoch := 0

	for c != 'k' {
		for i := range patterns {

			fmt.Println("=============")
			fmt.Println("Epoka", epoch)
			epoch++

			for n := range weights {
				printWeights(weights[n])
				delta := targets[n][i] - calcOut(weights[n], patterns[i])
				fmt.Printf("Delta %.3f\n", delta)

				for j := range patterns[i] {
					weights[n][j] = weights[n][j] + delta*learningRate*patterns[i][j]
				}
				weights[n][len(weights)-1] = weights[n][len(weights)-1] + delta*learningRate
			}

			fmt.Println("=============\n\n")
			fmt.Scanf("%c", &c)
			if c == 'k' {
				break
			}
		}
	}

	fmt.Println("Wzorce")
	for i := range patterns {
		for _, v := range patterns[i] {
			fmt.Printf("%.3f ", v)
		}
		fmt.Println();
	}

	fmt.Println("wartosci docelowe")
	fmt.Println(targets)

	return weights
}

func main() {

	stdin := bufio.NewReader(os.Stdin)
	fmt.Print("Podaj wspolczynnik uczenia: ")
	var learningRate float64 = 0.1
	//fmt.Scanf("%f", &learningRate)
	fmt.Println("wspolczynnik uczenia:",learningRate)
	fmt.Print("Podaj zakres wag: ")
	var wRange float64 = 1.0
	//fmt.Scanf("%f", &wRange)
	fmt.Println("Zakres wag:",wRange)

	weights := learn(wRange, learningRate)
	for i := range weights {
		printWeights(weights[i])
	}
	myPattern := []float64{2,2}
	fmt.Println("Podaj skladowe wlasnego wektora")
	fmt.Print("[1]: ")
	stdin.ReadString('\n')
	fmt.Scanf("%f", &myPattern[0])
	fmt.Print("[2]: ")
	fmt.Scanf("%f", &myPattern[1])

	for n := range weights {
		fmt.Println("Wynik", n, calcOut(weights[n], myPattern))
	}
}
