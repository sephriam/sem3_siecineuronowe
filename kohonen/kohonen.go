package main

import (
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"net/http"
	"os"
	"strconv"
)

type ClosestPoint struct {
	Distance float64
	X int
	Y int
}

func actFunc(x float64) float64 {
	return x
}

func calcOut(weights, pattern []float64) float64 {
	ret := 0.0
	////fmt.Println("x1", weights, "x2", pattern)
	for i, v := range weights {
		ret += (v - pattern[i])*(v - pattern[i])
	}
	////fmt.Println("dist:", math.Sqrt(ret))
	return actFunc(ret)
}

func printWeights(weights []float64) {
	fmt.Print("Wagi ")
	for i := range weights {
		fmt.Printf("%.3f ", weights[i])
	}
	fmt.Printf("\n")
}

var (
	weights [][][]float64
	learningRate = 0.002
	lowerBound = -10.0
	upperBound = 10.0
	neuronsH = 10
	neuronsV = 10
	neighboursFactor = 0.6
	epoch = 0
)

func loop() {

	//fmt.Println("=============")
	//fmt.Println("Epoka", epoch)
	epoch++
	// pattern loop
	x := []float64{rand.Float64()*(upperBound-lowerBound) + lowerBound, rand.Float64()*(upperBound-lowerBound) + lowerBound}
	//x := []float64{float64(rand.Int()%2)*2*upperBound + lowerBound, float64(rand.Int()%2)*2*upperBound + lowerBound}

	//fmt.Println("x", x)
	y := make([][]float64, neuronsV)
	for i := range y {
		y[i] = make([]float64, neuronsH)
	}
	closest := ClosestPoint{100000.0, 0,0}
	for i := 0; i < neuronsV; i++ {
		for j := 0; j < neuronsH; j++ {

			y[i][j] = calcOut(weights[i][j], x)

			if y[i][j] < closest.Distance {
				closest.Distance = y[i][j]
				closest.X = j
				closest.Y = i
				//fmt.Println("Point", i, j, closest.Distance)
			}
			//fmt.Print("Neuron ", i, ",", j, " ")
			//printWeights(weights[i][j])
		}
	}

	neighboursH := int(neighboursFactor*float64(neuronsH))
	neighboursV := int(neighboursFactor*float64(neuronsV))

	bI := closest.Y - neighboursV
	bJ := closest.X - neighboursH

	for i := bI; i < bI + (2*neighboursV+1); i++ {
		if i >= neuronsV || i < 0 {
			continue
		}
		for j := bJ; j < bJ + (2*neighboursH+1); j++ {
			if j >= neuronsH || j < 0  {
				continue
			}

			localNeighbourFactor := 1.0 - math.Sqrt(math.Pow(float64(i-closest.Y), 2) + math.Pow(float64(j-closest.X),2)) / math.Sqrt(math.Pow(float64(neighboursH),2) + math.Pow(float64(neighboursV),2))

			for k, v := range weights[i][j] {
				weights[i][j][k] = v + localNeighbourFactor*learningRate*(x[k]-weights[i][j][k])
			}
		}
	}
	//fmt.Println("=============\n\n")
}

type RequestHandler struct {
}

func (RequestHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	loop()

	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Set("Connection", "Keep-Alive")
	data, _ := json.Marshal(weights)
	w.Header().Set("Content-length", strconv.Itoa(len(data)))
	w.Write(data)
}

func main() {

	fmt.Println("Podaj wspolczynnik uczenia")
	fmt.Scanf("%f", &learningRate)
	fmt.Println(learningRate)
	fmt.Println("Podaj ilosc neuronow poziomo")
	fmt.Scanf("%d", &neuronsH)
	fmt.Println(neuronsH, "neuronow poziomo")

	fmt.Println("Podaj ilosc neuronow pionowo")
	fmt.Scanf("%d", &neuronsV)
	fmt.Println(neuronsH, "neuronow pionowo")

	weights = make([][][]float64, neuronsV)
	for i := 0; i < neuronsV; i++ {
		weights[i] = make([][]float64, neuronsH)
		for j := 0; j < neuronsH; j++ {
			//weights[i][j] = []float64{rand.Float64()*(upperBound-lowerBound) + lowerBound, rand.Float64()*(upperBound-lowerBound) + lowerBound}
			if len(os.Args) > 1 {
				neighboursFactor, _ = strconv.ParseFloat(os.Args[1], 64)
				weights[i][j] = []float64{float64(i) / float64(neuronsH), float64(j) / float64(neuronsV)}
			} else {
				weights[i][j] = []float64{0, 0}
			}
		}
	}

	handler := RequestHandler{}
	//http.HandleFunc("/", handler)
	server := &http.Server{Addr: ":8080", Handler: handler}
	server.SetKeepAlivesEnabled(true)
	server.ListenAndServe()
}