const domContainer = document.querySelector('#reactCanvas');
const e = React.createElement;
const width = 800
const height = 800
const itemSize = 10

class Counter extends React.Component {
    constructor() {
        super()
        this.counter = 0
        this.initPoints()
        this.interval = setInterval(() => {
            ReactDOM.render(this.render(), domContainer)
            this.update()
        }, 1)
    }

    stop() {
        clearInterval(this.interval)
    }

    render() {

        var c = document.getElementById("reactCanvas");
        var ctx = c.getContext("2d");
        ctx.clearRect(0, 0, c.width, c.height);
        for (var i = 0; i < this.points.length; i++) {
            for (var j = 0; j < this.points[i].length; j++) {

                if (i + 1 < this.points.length) {
                    ctx.beginPath();
                    ctx.moveTo((this.points[i][j][0] + itemSize) * width / (2*itemSize), (this.points[i][j][1] + itemSize) * height / (2*itemSize));
                    ctx.lineTo((this.points[i + 1][j][0] + itemSize) * width / (2*itemSize), (this.points[i + 1][j][1] + itemSize) * height / (2*itemSize));
                    ctx.stroke();
                }

                if (j + 1 < this.points[i].length) {
                    ctx.beginPath();
                    ctx.moveTo((this.points[i][j][0] + itemSize) * width / (2*itemSize), (this.points[i][j][1] + itemSize) * height / (2*itemSize));
                    ctx.lineTo((this.points[i][j + 1][0] + itemSize) * width / (2*itemSize), (this.points[i][j + 1][1] + itemSize) * height / (2*itemSize));
                    ctx.stroke();
                }

            }
        }
    }
    //return e('div', {style: { position: "absolute", left: (v2[0] + 10)*width/20, top: (v2[1] + 10)*height/20 }}, index+","+i2)
    // , "background-color": "black", width: itemSize+"px", height: itemSize+"px"

    update() {
        /*for(var i = 0; i < this.points.length; i++) {
            this.points[i].x += Math.round(Math.random()) * itemSize // = Math.floor(Math.random() * 800)
            this.points[i].y += Math.round(Math.random()) * itemSize// = Math.floor(Math.random() * 600)
            this.points[i].x %= width
            this.points[i].y %= height
        }*/
        return fetch('http://127.0.0.1:8080/')
            .then(results => results.json())
            .then((result) => {
                this.points = result
                console.log("this", this.points)
            })
    }

    initPoints() {
        return fetch('http://127.0.0.1:8080/')
            .then(results => results.json())
            .then((result) => {
                this.points = result
                console.log("this", this.points)
            })
    }
}
//ReactDOM.render(e(LikeButton), domContainer);
var counter = new Counter()