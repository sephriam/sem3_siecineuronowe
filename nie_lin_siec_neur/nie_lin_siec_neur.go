package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func normalize(vector []float64 ) []float64 {
	length := 0.0
	normalized := vector
	for _, v := range vector {
		length += v*v
	}
	length = math.Sqrt(length)
	for i, v := range normalized {
		normalized[i] = v / length
	}
	return normalized
}

func calcOut(weights, pattern []float64) float64 {
	ret := 0.0
	for i, v := range weights {
		ret += v * pattern[i]
	}

	if ret > 0.5 {
		return 1
	} else {
		return -1
	}
}

func printWeights(weights []float64) {
	fmt.Print("Wagi ")
	for i := range weights {
		fmt.Printf("%.3f ", weights[i])
	}
	fmt.Printf("\n")
}

func learn(wRange, learningRate float64) [][]float64 {

	patterns := [][]float64{{3,4,3,4,5}, {1,-2,1,-2,-2}, {-3,-2,-3,2,3}}
	targets := [][]float64{{1,-1,0.8}, {-1,0.8,-0.8,1}, {0.8,-0.8,1,-1}}
	weights := [][]float64{{-0.1,0.2,-0.5,0.3,-0.4}, {-0.1,0.2,-0.5,0.3,-0.4}}
	/*for i := range targets {
		for j := range targets[i] {
			weights[i][j] = (rand.Float64() * 2 * wRange) - wRange
		}
	}*/

	fmt.Println(patterns)
	fmt.Println(targets)
	for i := range weights {
		printWeights(weights[i])
	}

	for i, v := range patterns {
		patterns[i] = normalize(v)
	}

	var c uint8
	fmt.Scanf("%c", &c)
	epoch := 0

	for c != 'k' {
		for i := range patterns {

			fmt.Println("=============")
			fmt.Println("Epoka", epoch)
			epoch++

			for n := range weights {
				printWeights(weights[n])
				delta := targets[n][i] - calcOut(weights[n], patterns[i])
				fmt.Printf("Delta %.3f\n", delta)

				for j, vv := range weights[n] {
					weights[n][j] = vv + delta*learningRate*patterns[i][j]
				}
			}

			fmt.Println("=============\n\n")
			fmt.Scanf("%c", &c)
			if c == 'k' {
				break
			}
		}
	}

	fmt.Println("Wzorce")
	for i := range patterns {
		for _, v := range patterns[i] {
			fmt.Printf("%.3f ", v)
		}
		fmt.Println();
	}

	fmt.Println("wartosci docelowe")
	fmt.Println(targets)

	return weights
}

func main() {

	stdin := bufio.NewReader(os.Stdin)
	fmt.Print("Podaj wspolczynnik uczenia: ")
	var learningRate float64 = 0.1
	//fmt.Scanf("%f", &learningRate)
	fmt.Println("wspolczynnik uczenia:",learningRate)
	fmt.Print("Podaj zakres wag: ")
	var wRange float64 = 1.0
	//fmt.Scanf("%f", &wRange)
	fmt.Println("Zakres wag:",wRange)

	weights := learn(wRange, learningRate)
	for i := range weights {
		printWeights(weights[i])
	}
	myPattern := []float64{2,2,2,2,2}
	fmt.Println("Podaj skladowe wlasnego wektora")
	fmt.Print("[1]: ")
	stdin.ReadString('\n')
	fmt.Scanf("%f", &myPattern[0])
	fmt.Print("[2]: ")
	fmt.Scanf("%f", &myPattern[1])
	fmt.Print("[3]: ")
	fmt.Scanf("%f", &myPattern[2])
	fmt.Print("[4]: ")
	fmt.Scanf("%f", &myPattern[3])
	fmt.Print("[5]: ")
	fmt.Scanf("%f", &myPattern[4])

	myPattern = normalize(myPattern)

	for n := range weights {
		fmt.Println("Wynik", n, calcOut(weights[n], myPattern))
	}
}
