package main

import (
	"fmt"
	"math/rand"
)

func actFunc(x float64) float64 {
	return x
}

func calcOut(weights, pattern []float64) float64 {
	ret := 0.0
	for i, v := range weights {
		ret += v * pattern[i]
	}
	return actFunc(ret)
}

func printWeights(weights []float64) {
	fmt.Print("Wagi ")
	for i := range weights {
		fmt.Printf("%.3f ", weights[i])
	}
	fmt.Printf("\n")
}

func main() {

	learningRate := 0.01
	//fmt.Println("Podaj wspolczynnik uczenia")
	//fmt.Scanf("%f", &learningRate)
	fmt.Println(learningRate)

	lowerBound := -10.0
	upperBound := 10.0

	neuronsN := 4
	fmt.Println("Podaj ilosc neuronow")
	fmt.Scanf("%d", &neuronsN)
	fmt.Println(neuronsN, "neuronow")

	var weights [][]float64
	for i := 0; i < neuronsN; i++ {
		weights = append(weights, []float64{rand.Float64() * (upperBound - lowerBound) + lowerBound, rand.Float64() * (upperBound - lowerBound) + lowerBound})
	}

	c := 'o'
	epoch := 0
	for c != 'k' {

		fmt.Println("=============")
		fmt.Println("Epoka", epoch)
		epoch++
		// pattern loop
		x := []float64{float64((rand.Int() % 2) * 10) - 5.0, float64((rand.Int() % 2) * 10) - 5.0}
		fmt.Println("x", x)
		for i := range weights {

			y := calcOut(weights[i], x)

			nLearningRate := learningRate
			if y < 0 {
				nLearningRate *= 0.01
			}

			for j, v := range weights[i] {
				weights[i][j] = v + nLearningRate * y * (x[j]-v)
			}
			fmt.Print("Neuron ", i, " ")
			printWeights(weights[i])
		}
		fmt.Println("=============\n\n")
		fmt.Scanf("%c", &c)
	}
}
