package main

import (
	"fmt"
	"math"
)

func normalize(vector []float64 ) []float64 {
	length := 0.0
	normalized := vector
	for _, v := range vector {
		length += v*v
	}
	length = math.Sqrt(length)
	for i, v := range normalized {
		normalized[i] = v / length
	}
	return normalized
}

func calcOut(weights, pattern []float64) float64 {
	ret := 0.0
	for i, v := range weights {
		ret += v * pattern[i]
	}
	return ret
}

func printWeights(weights []float64) {
	fmt.Print("Wagi ")
	for i := range weights {
		fmt.Printf("%.3f ", weights[i])
	}
	fmt.Printf("\n")
}

func main() {

	learningRate := 0.1
	fmt.Println(learningRate)
	patterns := [][]float64{{2,2,2,2,2}, {-2,-2,-2,-2,-2}, {1,1,-1,-1,-1}, {-1,-1,1,1,1}}
	targets := []float64{1,-1,0.8,-0.8}
	weights := []float64{-0.1,0.2,-0.5,0.3,-0.4}

	fmt.Println(patterns)
	fmt.Println(targets)
	fmt.Println(weights)

	for i, v := range patterns {
		patterns[i] = normalize(v)
	}

	for c := 0; c < 30; c++ {
		for i := range patterns {

			fmt.Println("=============")
			printWeights(weights)
			fmt.Println("Epoka", c*len(patterns) + i)

			delta := targets[i] - calcOut(weights, patterns[i])
			fmt.Printf("Delta %.3f\n", delta)

			for j, vv := range weights {
				weights[j] = vv + delta*learningRate*patterns[i][j]
			}
			printWeights(weights)

			delta = targets[i] - calcOut(weights, patterns[i])
			fmt.Printf("Delta %.3f\n", delta)

			for j, vv := range weights {
				weights[j] = vv + delta*learningRate*patterns[i][j]
			}

			fmt.Println("=============\n\n")
			fmt.Scanln()
		}

	}
}
