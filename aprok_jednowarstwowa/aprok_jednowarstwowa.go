package main

import (
	"fmt"
	"math"
	"math/rand"
)

func actFunc(x float64) float64 {
	return (2.0 / (1.0 + math.Pow(math.E, -x))) - 1.0
}

func calcOut(weights, pattern []float64) float64 {
	ret := 0.0
	for i, v := range weights {
		ret += v * pattern[i]
	}
	return actFunc(ret)
}

func printWeights(weights []float64) {
	fmt.Print("Wagi ")
	for i := range weights {
		fmt.Printf("%.3f ", weights[i])
	}
	fmt.Printf("\n")
}

func main() {

	learningRate := 0.01
	fmt.Println("Podaj wspolczynnik uczenia")
	fmt.Scanf("%f", &learningRate)
	fmt.Println(learningRate)
	var x []float64
	var y []float64

	for i := 0; i < 14; i++ {
		x = append(x, float64(i)*0.4)
		y = append(y, math.Sin(x[i]))
	}

	var weights [][]float64
	for i := range x {
		weights = append(weights, []float64{})
		for range x {
			weights[i] = append(weights[i], rand.Float64())
		}
	}

	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(weights)

	c := 'o'
	epoch := 0
	for c != 'k' {

		fmt.Println("=============")
		fmt.Println("Epoka", epoch)
		epoch++
		for i := range x {
			delta := y[i] - calcOut(weights[i], x)
			fmt.Printf("Neuron %d delta %.3f\n", i, delta)
			for j, vv := range weights[i] {
				weights[i][j] = vv + delta*learningRate*x[j]
			}
			//printWeights(weights[i])
		}
		fmt.Println("=============\n\n")
		fmt.Scanf("%c", &c)
	}

	fmt.Scanln()
	fmt.Println("o ile przesunac wzorzec?")
	move := 0.2
	fmt.Scanf("%f", &move)

	for i := range x {
		x[i] += move
	}

	fmt.Println("=============")
	for i := range x {
		delta := y[i] - calcOut(weights[i], x)
		fmt.Printf("Neuron %d delta %.3f\n", i, delta)
		for j, vv := range weights[i] {
			weights[i][j] = vv + delta*learningRate*x[j]
		}
		//printWeights(weights[i])
	}
	fmt.Println("=============\n\n")

}
