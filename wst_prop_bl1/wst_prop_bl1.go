package main

import (
	"fmt"
	"math"
)

func actFunc(x float64) float64 {
	return 1.0 / (1.0 + math.Pow(math.E, -x))
}

func actFuncDeriv(x float64) float64 {
	return (1-x)*x
}

func calcOut(weights, pattern []float64) float64 {
	ret := 0.0
	for i, v := range weights {
		ret += v * pattern[i]
	}
	return actFunc(ret)
}

func calcOutLin(weights, pattern []float64) float64 {
	ret := 0.0
	for i, v := range weights {
		ret += v * pattern[i]
	}
	return ret
}

func printWeights(weights []float64) {
	fmt.Print("Wagi ")
	for i := range weights {
		fmt.Printf("%.3f ", weights[i])
	}
	fmt.Printf("\n")
}

func main() {

	learningRate := 0.01
	fmt.Println("Podaj wspolczynnik uczenia")
	fmt.Scanf("%f", &learningRate)
	fmt.Println(learningRate)
	x := [][]float64{{0,0,0,0,0},{1,1,1,1,1},{1,1,0,0,0},{1,0,1,0,1}}
	z := []float64{1, 0, 0.3, 0.7}

	weights0 := [][]float64{{4,-3,0.5,1,0.7}, {3,0.5,-2,0.7,1}}
	weights1 := []float64{-0.4,-2}

	c := 'o'
	epoch := 0
	for c != 'k' {

		fmt.Println("=============")
		fmt.Println("Epoka", epoch)
		epoch++
		// pattern loop
		for i := range x {

			var y []float64
			// get y values for i-th pattern
			for j := range weights0 {
				y = append(y, calcOut(weights0[j], x[i]))
			}

			deltaz := z[i] - calcOutLin(weights1, y)
			fmt.Println("Blad dla wzorca", i, "wynosi", deltaz)
			for j, vv := range weights1 {
				weights1[j] = vv + learningRate * deltaz * y[j]
			}

			for j := range weights1 {
				for k := range weights0 {
					//(1 - y[i]) * y[i]
					weights0[j][k] = weights0[j][k] + learningRate * deltaz * y[j] * actFuncDeriv(y[j]) * x[i][k]
				}
			}
		}
		fmt.Println("=============\n\n")
		fmt.Scanf("%c", &c)
	}
}
